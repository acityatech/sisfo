<script src="{{asset('assets/assets/node_modules/jquery/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('assets/assets/node_modules/popper/popper.min.js')}}"></script>
<script src="{{asset('assets/assets/node_modules/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/dist/js/perfect-scrollbar.jquery.min.js')}}"></script>
<script src="{{asset('assets/dist/js/waves.js')}}"></script>
<script src="{{asset('assets/dist/js/sidebarmenu.js')}}"></script>
<script src="{{asset('assets/dist/js/custom.min.js')}}"></script>
<script src="{{asset('assets/assets/node_modules/datatables/jquery.dataTables.min.js')}}"></script>
@yield('script')
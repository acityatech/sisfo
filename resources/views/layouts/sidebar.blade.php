<aside class="left-sidebar">
    <div class="d-flex no-block nav-text-box align-items-center">
        <span><img src="{{asset('assets/assets/images/logo-icon.png')}}" alt="elegant admin template"></span>
        <a class="nav-lock waves-effect waves-dark ml-auto hidden-md-down" href="javascript:void(0)"><i class="mdi mdi-toggle-switch"></i></a>
        <a class="nav-toggler waves-effect waves-dark ml-auto hidden-sm-up" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
    </div>
    
    <div class="scroll-sidebar">
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                @if (Auth::guard('siswa')->check())
                    @include('layouts.sidebar_siswa')
                @elseif(Auth::guard('guru')->check())
                    @include('layouts.sidebar_guru')
                @elseif(Auth::guard('admin')->check())
                    @include('layouts.sidebar_admin')
                @endif
            </ul>
        </nav>
    </div>
</aside>
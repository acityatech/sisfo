<li> <a class="waves-effect waves-dark" href="{{route('admin.dashboard')}}" aria-expanded="false"><i class="icon-speedometer"></i><span class="hide-menu">Dashboard</span></a></li>
<li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="icon-layers"></i><span class="hide-menu">Data Master</span></a>
    <ul aria-expanded="false" class="collapse">
        <li><a href="{{route('admin.master.identitas_sekolah.index')}}">Identitas Sekolah</a></li>
        <li><a href="{{route('admin.master.kurikulum.index')}}">Kurikulum</a></li>
        <li><a href="{{route('admin.master.tahun_akademik.index')}}">Tahun Akademik</a></li>
        <li><a href="{{route('admin.master.gedung.index')}}">Gedung</a></li>
        <li><a href="{{route('admin.master.ruangan.index')}}">Ruangan</a></li>
        <li><a href="{{route('admin.master.golongan.index')}}">Golongan</a></li>
        <li><a href="{{route('admin.master.ptk.index')}}">Jenis PTK</a></li>
        <li><a href="{{route('admin.master.jurusan.index')}}">Jurusan</a></li>
        <li><a href="{{route('admin.master.kelas.index')}}">Kelas</a></li>
        <li><a href="{{route('admin.master.status_pegawai.index')}}">Status Pegawai</a></li>
    </ul>
</li>
<li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="icon-user"></i><span class="hide-menu">Data Pengguna</span></a>
    <ul aria-expanded="false" class="collapse">
        <li><a href="{{route('admin.pengguna.siswa.index')}}">Siswa</a></li>
        <li><a href="{{route('admin.pengguna.guru.index')}}">Guru</a></li>
        <li><a href="{{route('admin.pengguna.admin.index')}}">Administrator</a></li>
    </ul>
</li>
<li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="icon-graduation"></i><span class="hide-menu">Data Akademik</span></a>
    <ul aria-expanded="false" class="collapse">
        <li><a href="{{route('admin.akademik.kelompok_mapel.index')}}">Kelompok Mapel</a></li>
        <li><a href="{{route('admin.akademik.mapel.index')}}">Mata Pelajaran</a></li>
        <li><a href="{{route('admin.akademik.jadpel.index')}}">Jadwal Pelajaran</a></li>
        <li><a href="{{route('admin.akademik.bahantugas.index')}}">Bahan dan Tugas</a></li>
        <li><a href="{{route('admin.akademik.kompetensi_dasar.index')}}">Kompetensi Dasar</a></li>
        <li><a href="{{route('admin.akademik.penilaian_diri.index')}}">Penilaian Diri</a></li>
        <li><a href="{{route('admin.akademik.rentang_nilai.index')}}">Rentang Nilai</a></li>
    </ul>
</li>
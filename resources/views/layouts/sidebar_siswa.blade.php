<li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="icon-speedometer"></i><span class="hide-menu">Dashboard <span class="badge badge-pill badge-cyan">4</span></span></a>
    <ul aria-expanded="false" class="collapse">
        <li><a href="index.html">Minimal <i class="fa fa-circle-o text-success"></i></a></li>
        <li><a href="index2.html">Analytical <i class="fa fa-circle-o text-info"></i></a></li>
        <li><a href="index3.html">Demographical <i class="fa fa-circle-o text-danger"></i></a></li>
        <li><a href="index4.html">Modern <i class="fa fa-circle-o text-warning"></i></a></li>
        <li><a href="index5.html">Cryptocurrency <i class="fa fa-circle-o text-primary"></i></a></li>
    </ul>
</li>
<li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="ti-layout-grid2"></i><span class="hide-menu">Apps</span></a>
    <ul aria-expanded="false" class="collapse">
        <li><a href="app-calendar.html">Calendar <i class="ti-calendar"></i></a></li>
        <li><a href="app-chat.html">Chat app <i class="ti-comment"></i></a></li>
        <li><a href="app-ticket.html">Support Ticket <i class="ti-support"></i></a></li>
        <li><a href="app-contact.html">Contact / Employee <i class="ti-user"></i></a></li>
        <li><a href="app-contact2.html">Contact Grid <i class=" ti-list"></i></a></li>
        <li><a href="app-contact-detail.html">Contact Detail <i class="ti-pencil-alt"></i></a></li>
    </ul>
</li>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('favicon.ico')}}">
    <title>SISFO</title>
    @include('layouts.master_css')
</head>

<body class="skin-red fixed-layout lock-nav">
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
        </div>
    </div>
    
    <div id="main-wrapper">
        @include('layouts.header')

        @include('layouts.sidebar')
    
        <div class="page-wrapper">
            <div class="container-fluid">

                @yield('content')

            </div>
        </div>
        @include('layouts.footer')
    </div>
    @include('layouts.master_js')
</body>

</html>
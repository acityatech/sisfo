<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    protected $username = 'username';

    public function __construct()
    {        
        $this->middleware('guest')->except('logout');
        $this->username = $this->findUsername();
    }

    public function findUsername()
    {
        $login = request()->input('username');
 
        $fieldType = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
 
        request()->merge([$fieldType => $login]);
 
        return $fieldType;
    }

    public function username()
    {
        return $this->username;
    }

    public function login(Request $request){
        if($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'username'      => 'required',
                'password'      => 'required|alphaNum',
            ]);
            if ($validator->fails()) {
                return redirect()->route('admin.login')
                                ->withErrors($validator)
                                ->withInput()
                                ->with('alert','Email/Username/Password requered!');
            }
            $credentials = $request->only('username', 'password');

            if(Auth::guard('admin')->attempt([$this->username() => $credentials['username'], 'password' => $credentials['password']])){
                return redirect()->route('admin.dashboard');
            }else{
                return redirect()->route('admin.login')
                                    ->withInput()
                                    ->with('alert','Username/Email/Password wrong!');
            }
        }else{
            return view('admin.login');
        }
    }

    public function logout(){
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login')->with('success', 'You are Logout!');
    }
}

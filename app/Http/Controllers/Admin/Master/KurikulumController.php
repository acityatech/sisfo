<?php

namespace App\Http\Controllers\Admin\Master;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class KurikulumController extends Controller
{
    public function __construct()
    {        
        $this->middleware('admin');
    }

    public function index()
    {
        return view('admin.master.kurikulum.index');
    }

    public function save(Request $request)
    {
        return $this->username;
    }
}

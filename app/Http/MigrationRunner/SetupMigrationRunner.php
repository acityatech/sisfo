<?php

namespace App\Http\MigrationRunner;

class SetupMigrationRunner
{
	public function run()
	{
		// Master
        $KurikulumMigration = new \App\Http\Migration\KurikulumMigration();
		$KurikulumMigration->createTable();
		
		// Pengguna
        $AdminMigration = new \App\Http\Migration\AdminMigration();
        $AdminMigration->createTable();
	}
}
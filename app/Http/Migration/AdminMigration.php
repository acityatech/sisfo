<?php

namespace App\Http\Migration;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdminMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function createTable()
    {
        if(!Schema::hasTable('admin'))
        {
            Schema::create('admin', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name');
                $table->string('username',50)->unique();
                $table->string('email',50)->unique();
                $table->string('password',500);
                $table->timestamps();
            });

            DB::table('admin')->insert([
                [   'id'            => 1,
                    'name'          => 'Administrator',
                    'username'      => 'admin',
                    'email'         => 'admin@gmail.com',
                    'password'      => Hash::make('123456'),
                    'created_at'    => null,
                    'updated_at'    => null
                ]
            ]);
        }
    }
}
<?php

namespace App\Http\Migration;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KurikulumMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function createTable()
    {
        if(!Schema::hasTable('kurikulum'))
        {
            Schema::create('kurikulum', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('nama');
                $table->boolean('active')->default(false);
                $table->timestamps();
            });
        }
    }
}
<?php

Route::group(['namespace' => 'Admin'] , function(){
    Route::get('/login', 'AuthController@login')->name('admin.login');
    Route::post('/login', 'AuthController@login')->name('admin.login-post');
    Route::get('/logout', 'AuthController@logout')->name('admin.logout');

    Route::get('/', function () {
        if (Auth::guard('admin')->check()) {
            return redirect()->route('admin.dashboard');
        }
        return abort(404);
    });


    Route::group(['middleware' => 'admin'] , function(){
        Route::get('/dashboard', 'DashboardController@index')->name('admin.dashboard');
        Route::group(['prefix' => 'master','namespace' => 'Master'] , function(){
            Route::group(['prefix' => 'identitas'] , function(){
                Route::get('/', 'IdentitasSekolahController@index')->name('admin.master.identitas_sekolah.index');
                Route::get('/save', 'IdentitasSekolahController@save')->name('admin.master.identitas_sekolah.save');
            });
            Route::group(['prefix' => 'kurikulum'] , function(){
                Route::get('/', 'KurikulumController@index')->name('admin.master.kurikulum.index');
                Route::get('/save', 'KurikulumController@save')->name('admin.master.kurikulum.save');
            });
            Route::group(['prefix' => 'tahunakademik'] , function(){
                Route::get('/', 'AkademikController@index')->name('admin.master.tahun_akademik.index');
                Route::get('/save', 'AkademikController@save')->name('admin.master.tahun_akademik.save');
            });
            Route::group(['prefix' => 'gedung'] , function(){
                Route::get('/', 'GedungController@index')->name('admin.master.gedung.index');
                Route::get('/save', 'GedungController@save')->name('admin.master.gedung.save');
            });
            Route::group(['prefix' => 'ruangan'] , function(){
                Route::get('/', 'RuanganController@index')->name('admin.master.ruangan.index');
                Route::get('/save', 'RuanganController@save')->name('admin.master.ruangan.save');
            });
            Route::group(['prefix' => 'golongan'] , function(){
                Route::get('/', 'Golonganontroller@index')->name('admin.master.golongan.index');
                Route::get('/save', 'Golonganontroller@save')->name('admin.master.golongan.save');
            });
            Route::group(['prefix' => 'ptk'] , function(){
                Route::get('/', 'PtkController@index')->name('admin.master.ptk.index');
                Route::get('/save', 'PtkController@save')->name('admin.master.ptk.save');
            });
            Route::group(['prefix' => 'jurusan'] , function(){
                Route::get('/', 'JurusanController@index')->name('admin.master.jurusan.index');
                Route::get('/save', 'JurusanController@save')->name('admin.master.jurusan.save');
            });
            Route::group(['prefix' => 'kelas'] , function(){
                Route::get('/', 'KelasController@index')->name('admin.master.kelas.index');
                Route::get('/save', 'KelasController@save')->name('admin.master.kelas.save');
            });
            Route::group(['prefix' => 'statuspegawai'] , function(){
                Route::get('/', 'StatusPegawaiController@index')->name('admin.master.status_pegawai.index');
                Route::get('/save', 'StatusPegawaiController@save')->name('admin.master.status_pegawai.save');
            });
        });

        Route::group(['prefix' => 'pengguna','namespace' => 'Pengguna'] , function(){
            Route::group(['prefix' => 'siswa'] , function(){
                Route::get('/', 'IdentitasController@index')->name('admin.pengguna.siswa.index');
                Route::get('/save', 'IdentitasController@save')->name('admin.pengguna.siswa.save');
            });
            Route::group(['prefix' => 'guru'] , function(){
                Route::get('/', 'KurikulumController@index')->name('admin.pengguna.guru.index');
                Route::get('/save', 'KurikulumController@save')->name('admin.pengguna.guru.save');
            });
            Route::group(['prefix' => 'admin'] , function(){
                Route::get('/', 'AdminController@index')->name('admin.pengguna.admin.index');
                Route::get('/save', 'AdminController@save')->name('admin.pengguna.admin.save');
            });
        });

        Route::group(['prefix' => 'akademik','namespace' => 'Akademik'] , function(){
            Route::group(['prefix' => 'kelompokmapel'] , function(){
                Route::get('/', 'KelompokMapelController@index')->name('admin.akademik.kelompok_mapel.index');
                Route::get('/save', 'KelompokMapelController@save')->name('admin.akademik.kelompok_mapel.save');
            });
            Route::group(['prefix' => 'mapel'] , function(){
                Route::get('/', 'MataPelajaranController@index')->name('admin.akademik.mapel.index');
                Route::get('/save', 'MataPelajaranController@save')->name('admin.akademik.mapel.save');
            });
            Route::group(['prefix' => 'jadpel'] , function(){
                Route::get('/', 'JadwalPelajaranController@index')->name('admin.akademik.jadpel.index');
                Route::get('/save', 'JadwalPelajaranController@save')->name('admin.akademik.jadpel.save');
            });
            Route::group(['prefix' => 'bahantugas'] , function(){
                Route::get('/', 'BahanTugasController@index')->name('admin.akademik.bahantugas.index');
                Route::get('/save', 'BahanTugasController@save')->name('admin.akademik.bahantugas.save');
            });
            Route::group(['prefix' => 'kompetensidasar'] , function(){
                Route::get('/', 'KompetensiDasarController@index')->name('admin.akademik.kompetensi_dasar.index');
                Route::get('/save', 'KompetensiDasarController@save')->name('admin.akademik.kompetensi_dasar.save');
            });
            Route::group(['prefix' => 'penilaiandiri'] , function(){
                Route::get('/', 'PenilaianDiriController@index')->name('admin.akademik.penilaian_diri.index');
                Route::get('/save', 'PenilaianDiriController@save')->name('admin.akademik.penilaian_diri.save');
            });
            Route::group(['prefix' => 'rentangnilai'] , function(){
                Route::get('/', 'RentangNilaiController@index')->name('admin.akademik.rentang_nilai.index');
                Route::get('/save', 'RentangNilaiController@save')->name('admin.akademik.rentang_nilai.save');
            });
        });
    });
});
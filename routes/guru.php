<?php

Route::group(['prefix' => 'guru','namespace' => 'Guru','middleware' => ['guru']] , function(){
    Route::get('/', 'HomeController@index')->name('guru.index');
});
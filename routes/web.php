<?php

Route::get('/', 'AuthController@index')->name('login');
Route::post('/login', 'AuthController@login')->name('login_post');
Route::get('/logout', 'AuthController@logout')->name('logout');

Route::get('/migrate',function() {
	$setupMigrationRunner = new App\Http\MigrationRunner\SetupMigrationRunner();
	$setupMigrationRunner->run($this);
});

Route::get('/welcome',function() {
	return view('welcome');
});